# Unsplash Desktop Wallpaper

Linux bash script to fetch random HD image from unsplash and set it as a wallpaper.

**How to use it?**

- Download the bash file either by cloning the repo or manually copying its content to your system
- Give the file execute permissions ($ chmod a+x unsplash.sh)
- Execute the file using terminal ($ ./unsplash.sh). 

**Tested and works on,**
- Ubuntu 16.04