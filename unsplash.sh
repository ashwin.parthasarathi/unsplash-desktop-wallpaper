#/bin/bash
echo "Starting Unsplash processing.";
#new_link=$( curl -Ls -o /dev/null -w %{url_effective} https://unsplash.it/2560/1440/\?random )
new_link=$( curl -Ls -o /dev/null -w %{url_effective} https://source.unsplash.com/random/2560x1440 )
echo $new_link
cut="$(echo $new_link | cut -d'/' -f4)";
cut="$(echo $cut | cut -d'?' -f1)";
fn="Unsplash-$(echo $cut | sed 's,/,-,g').jpg";
echo "Fetching Image from Unsplash.";
wget -q -O $HOME/Pictures/$fn $new_link
echo "Setting fetched image."
gsettings set org.gnome.desktop.background picture-uri file://$HOME/Pictures/$fn
echo "Image set. $cut"
